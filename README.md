# PFX Generador #

Aplicación de consola para la generación del archivo PFX para la cancelación de un CFDI en la version 3.3

### Requerimientos ###

* Solución en C#
* 1.0
* Se requiere OPENSSL
* Se requiere archivos CSD
* Se requiere la clave privada del archivo key

### Contactanos ###

* uirtusit.com
* rene@uirtusit.com