﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        private static PFX oPFX;
        private static string cer = @"C:\Users\Administrator\Documents\CSD_Pruebas\CSD_Pruebas_CFDI_LAN7008173R5.cer";
        private static string key = @"C:\Users\Administrator\Documents\CSD_Pruebas\CSD_Pruebas_CFDI_LAN7008173R5.key";
        private static string clave = "12345678a";
        private static string pfx = @"C:\Users\Administrator\Documents\CSD_Pruebas\CSD_Pruebas_CFDI_LAN7008173R5.pfx";
        private static string path = @"C:\Users\Administrator\Documents\CSD_Pruebas\";

        static void Main(string[] args)
        {
            oPFX = new PFX(cer, key, clave, pfx, path);

            if (oPFX.creaPFX())
                Console.WriteLine("Archivo creado con exito");
            else
                Console.WriteLine(oPFX.error);
        }
    }
}
